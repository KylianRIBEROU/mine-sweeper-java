import java.util.List;
import java.util.ArrayList;
import java.util.Random;

public class Plateau{
    
    private int nbLignes;
    private int nbColonnes;
    private int pourcentageDeBombes;
    private int nbBombes;
    protected List<CaseIntelligente> lePlateau = new ArrayList<>();
    /**
     * constructeur de la classe Plateau
     * @param nbL nombre de ligne du plateau
     * @param nbC nombre de colonne du plateau
     * @param pourcentageDeBombes le pourcentage de bombes sur tout le plateau
     */
    public Plateau(int nbL, int nbC, int pourcentageDeBombes){
        this.nbLignes=nbL;
        this.nbColonnes=nbC;
        this.pourcentageDeBombes=pourcentageDeBombes;
        
    }
    /**
     * méthode permettant de connaitre le nombre de ligne du plateau
     * @return le nombre de ligne
     */
    public int getNbLignes(){
        return this.nbLignes;
    }
    /**
     * méthode permettant de connaitre le nombre de Colonne du plateau
     * @return le nombre de Colonne
     */
    public int getNbColonnes(){
        return this.nbColonnes;
    }
    /**
     * méthode permettant de créer des case vide
     */
    public void creerLesCasesVides(){
        for(int i=0; i<this.getNbLignes()*this.getNbColonnes();++i){
            this.lePlateau.add(new CaseIntelligente());
        }
    }
    /**
     * méthode permettant le nombre total de bombe sur le plateau
     * @return le nombre de bombe sur le plateau
     */
    public int getNbTotalBombes(){
        return this.nbBombes;
    }
    /**
     * méthode permettant de selectionner une case du plateau
     * @param numLigne position en x de la case voulu
     * @param numColonne position en y de la case voulu
     * @return la case en coordonner (x,y)
     */
    public CaseIntelligente getCase(int numLigne, int numColonne){
        try {
            return this.lePlateau.get(this.nbColonnes*numLigne+numColonne);
        }
        catch (IndexOutOfBoundsException ignored){
            return null;
        }
    }
    /**
     * renvoie le nombre de case marquée
     * @return le nombre de case marquée
     */
    public int getNbCasesMarquees(){
        int nbMarquees=0;
        for (CaseIntelligente c:this.lePlateau){
            if (c.estMarquee){
                nbMarquees+=1;
            }
        }
        return nbMarquees;
    }
    /**
     * pose une bombe dans une case
     * @param x la position en x de la case
     * @param y la position en y de la case
     */
    public void poseBombe(int x, int y){
        this.getCase(x, y).poseBombe();
    }
    /**
     * reset toute les cases du plateau 
     */
    public void reset(){
        if (this.lePlateau.isEmpty()){
            this.creerLesCasesVides();
        }
        for (CaseIntelligente c : this.lePlateau){
            c.reset();
            }
         
        this.nbBombes=0;
        this.rendLesCasesintelligentes();
        this.poseDesBombesAleatoirement();
    
    }


    /**
     * Méthode qui, pour chaque case du plateau, va ajouter ses cases voisines dans la liste LesVoisines de cette case
     */
    public void rendLesCasesintelligentes(){
        int Col = getNbColonnes();
        int lig = getNbLignes();
        for(int aLig=0; aLig<lig; ++aLig){
            for(int aCol=0; aCol<Col; ++aCol){
                for(int a=-1;a<=1;++a){
                    for(int b=-1;b<=1;++b){
                        if(aLig+a>=0 && aLig+a<lig){
                            if(aCol+b>=0 && aCol+b<Col){
                                if(aLig+a != aLig || aCol+b != aCol){
                                    this.getCase(aLig,aCol).ajouteVoisine(this.getCase(aLig+a,aCol+b));
                                }
                            }
                        }
                    }
                }
            } 
        }
    }

    /**
     * pose des bombes aléatoiremnt dans le plateau
     */
    protected void poseDesBombesAleatoirement(){
        Random generateur = new Random();
        for (int x = 0; x < this.getNbLignes(); x++){
            for (int y = 0; y < this.getNbColonnes(); y++){
                if (generateur.nextInt(100)+1 < this.pourcentageDeBombes){
                    this.poseBombe(x, y);
                    this.nbBombes = this.nbBombes + 1;
                }
            }
        }
    }
}
