public class Case {
    public boolean contientUneBombe;
    public boolean estDecouverte;
    public boolean estMarquee;
    /**
     * constructeur de la classe Case
     */
    public Case(){
        this.reset();
    }
    /**
     * permet de reset une case en mettant a false tout ces attributs
     */
    public void reset(){
        this.contientUneBombe=false;
        this.estDecouverte=false;
        this.estMarquee=false;
    }
    /**
     * méthode permettant de poser une bombe dasn une case
     */
    public void poseBombe(){
        this.contientUneBombe=true;
    }
    /**
     * méthode permettant de savoir si la case contient une bombe ou non
     * @return true si oui et false si non
     */
    public boolean contientUneBombe(){
        return this.contientUneBombe;
    }
    /**
     * méthode permettant de de savoir si la case est découverte ou non
     * @return true si oui et false si non
     */
    public boolean estDecouverte(){
        return this.estDecouverte;
    }
    /**
     * méthode permettant de de savoir si la case est marquée ou non
     * @return true si oui et false si non
     */
    public boolean estMarquee(){
        return this.estMarquee;
    }
    /**
     * méthode permettant de reveler une case
     * @return true si la case n'étaient pas rveler avant et false si non 
     */
    public boolean reveler(){
        if (!this.estDecouverte){
            this.estDecouverte=true;
            this.estMarquee=false;
            return true;
        }
        return false; 
    }
    /**
     * méthode permettant de marquer une case 
     */
    public void marquer(){
        if ( !this.estDecouverte){
            if ( this.estMarquee ){
                this.estMarquee=false;
                this.estDecouverte=false;
                }              
            else {      // si la case est déjà marquée, on peut lui enlever la marque. Si on essaye de marquer une case découverte,
            this.estMarquee=true;  // il ne se passera rien car ce n'est pas possible 
            }
        }
        else{
            this.estMarquee=false;
        }
    }   
}
