import java.util.Scanner;

public class Demineur extends Plateau{
    private int score;
    private boolean gameOver = false;
    /**
     * constructeur de la classe Demineur
     * @param nbL nombre de ligne 
     * @param nbC nombre de colonne
     * @param pourcentage le pourcentage de bombe dans la partie
     */
    public Demineur(int nbL, int nbC, int pourcentage){
        super(nbL, nbC, pourcentage);
        this.gameOver=false;
        this.score=0;
        
    }
    /**
     * méthode retournant le score de la partie 
     * @return le score de la partie
     */
    public int getScore(){
        return this.score;
    }
    /**
     * méthode permettant de reveler une case du plateau
     * @param x la position en x de la case 
     * @param y la position en y de la case 
     */
    public void reveler(int x, int y){
        boolean a=this.lePlateau.get(super.getNbColonnes()*x+y).reveler();
        if (a){
            score+=10;
        }
        
    }
    /**
     *méthode permettant de marquer une case du plateau
     * @param x la position en x de la case 
     * @param y la position en y de la case 
     * @return
     */
    public int marquer(int x, int y){
        this.lePlateau.get(super.getNbColonnes()*x+y).marquer();
        return super.getNbColonnes()*x+y;
    }
    /**
     * méthode permettant de savoir si la partie est gagnée
     * @return true si la partie est gagnée et false si non
     */
    public boolean estGagnee(){
        int total = 0;
        for (CaseIntelligente c:this.lePlateau){
            if (c.estDecouverte() && !c.contientUneBombe()){
                total +=1;
            }
        }
        return total >= this.getNbLignes()*this.getNbColonnes()-this.getNbTotalBombes();
    }
    /**
     * méthode permettant de savoir si la parti est perdu
     * @return true si la partie est perdu et false si non
     */
    public boolean estPerdue(){
        for(CaseIntelligente c : lePlateau){
            if(c.estDecouverte() &&  c.contientUneBombe()){
                this.gameOver=true;
                return gameOver;
            }
        }
        return gameOver;
    }
    /**
     * méthode permettant de reset une partie
     */
    public void reset(){
        this.gameOver=false;
        this.score=0;
        super.reset();
    
    }
    /**
     * méthode permettant d'afficher le jeux en cours sur le terminal
     */
    public void affiche(){
        System.out.println("JEU DU DEMINEUR");
        // affichage de la bordure supérieure
        System.out.print("  ");
        for (int j=0; j<this.getNbColonnes(); j++){
            System.out.print("  "+j+" ");
        }
        System.out.print(" \n");
        
        // affichage des numéros de ligne + cases
        System.out.print("  ┌");
        for (int j=0; j<this.getNbColonnes()-1; j++){
                System.out.print("───┬");
        }
        System.out.println("───┐");
        
        // affichage des numéros de ligne + cases
        for (int i = 0; i<this.getNbLignes(); i++){
            System.out.print(i+" ");
            for (int j=0; j<this.getNbColonnes(); j++){
                System.out.print("│ "+this.getCase(i, j).toString() + " ");
            }
            System.out.print("│\n");
            
            if (i!=this.getNbLignes()-1){
                // ligne milieu
                System.out.print("  ├");
                for (int j=0; j<this.getNbColonnes()-1; j++){
                        System.out.print("───┼");
                }
                System.out.println("───┤");
            }
        }

        // affichage de la bordure inférieure
        System.out.print("  └");
        for (int j=0; j<this.getNbColonnes()-1; j++){
                System.out.print("───┴");
        }
        System.out.println("───┘");
        
        // affichage des infos 
        System.out.println("Nombres de bombes à trouver : " + this.getNbTotalBombes());
        System.out.println("Nombres de cases marquées : " + this.getNbCasesMarquees());
        System.out.println("Score : " + this.getScore());
    }

    /**
     * méthode permettant de démarrer une nouvelle partie
     */
    public void nouvellePartie(){
        this.reset();
        this.affiche();
        Scanner scan = new Scanner(System.in).useDelimiter("\n");

        while (!this.estPerdue() && !this.estGagnee()){
            System.out.println("Entrer une instruction de la forme R 3 2 ou M 3 2\npour Révéler/Marquer la case à la ligne 3 et à la colonne 2");
            String [] s = scan.nextLine().split(" ");
            String action = s[0];
            int x = Integer.valueOf(s[1]);
            int y = Integer.valueOf(s[2]);
            if (action.equals("M") || action.equals("m"))
                this.marquer(x, y);
            else if (action.equals("R") || action.equals("r"))
                this.reveler(x, y);
            this.affiche();
        }
        if (this.gameOver){
            System.out.println("Oh !!! Vous avez perdu !");
        }
        else{
            System.out.println("Bravo !! Vous avez gagné !");
        }
    }
}
