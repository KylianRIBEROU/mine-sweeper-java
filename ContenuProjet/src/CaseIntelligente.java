
import java.util.ArrayList;
import java.util.List;

public class CaseIntelligente extends Case{
    protected List <Case> lesVoisines;
    
    /**
     * constructeur de la classe CaseIntelligente
     */
    public CaseIntelligente(){
        super();
        lesVoisines= new ArrayList<>();
    }
    /**
     * permet d'ajouter une case a la liste des cases voisines
     * @param c une case de lePlateau
     */
    public void ajouteVoisine(Case c){
        this.lesVoisines.add(c);
    }
    /**
     * méthode renvoyant le nombre de bombe autour de la case visée( dans un rayon de 1 en diagonale,verticale et horizontale)
     * @return le nonbre de bombe dans les case voisine
     */
    public int nombreBombesVoisines(){
        int nbBombes = 0;
        for (Case c:this.lesVoisines){
            if(c.contientUneBombe()){
                nbBombes+=1;
            }
        }
        return nbBombes;
    }

    /**
     * méthode qui renvoie un symbole String correspondant au statut de la case, selon si elle contient une bombe ou qu'elle est marquée, ou qu'elle n'est pas découverte
     * @return String : un string 
     */
    @Override
    public String toString(){
        
        if (this.estMarquee){
            return "?";
        }
        else if(this.estDecouverte){
            if (this.contientUneBombe){ return "@";}
            else{
                return ""+this.nombreBombesVoisines();
                
            }
        }

        else{
            return " ";
        }

    }

}
