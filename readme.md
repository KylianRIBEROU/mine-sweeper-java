Projet réalisé en groupe par :
- RIBEROU Kylian
- CANNIER Gillian
- SIMON Gaël

Pour lancer le démineur, si le script ne fonctionne pas, veuillez utiliser ces commandes depuis le répertoire parent de src.
Ces commandes sont adaptées à une exécution sur windows.

Si vous souhaitez exécuter le fichier sur votre ordinateur, veuillez modifier le lien vers la bibliothèque de javaFX et les backslash par des slahs si vous etes sur Linux

compiler : javac -encoding UTF-8 -d bin --module-path C:\Users\kylia\Documents\packages\javafx-sdk-20\lib\ --add-modules javafx.controls .\src\*.java     

exécuter : java --module-path C:\Users\kylia\Documents\packages\javafx-sdk-20\lib\ --add-modules javafx.controls --class-path .\bin\ .\src\Executable.java